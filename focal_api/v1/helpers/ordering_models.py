from pydantic import BaseModel


class FeasibilityEstimateModel(BaseModel):
    limit: int
    lengthOfInterview: int
