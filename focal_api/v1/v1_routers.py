from fastapi import APIRouter

from focal_api.v1.routers import ordering

router = APIRouter(prefix="/v1", tags=["v1"])

router.include_router(ordering.router)
