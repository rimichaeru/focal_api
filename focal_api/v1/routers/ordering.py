import os

import aiohttp
from fastapi import APIRouter, Depends, HTTPException

from focal_api.v1.helpers.ordering_models import FeasibilityEstimateModel

# FIELD_PERIOD_PAIRS, defined in days
FIELD_PERIOD_PAIRS: dict = {1: "Day", 7: "Week", 30: "Month"}
CINT_API_ENDPOINT = "https://api.cintworks.net/ordering/FeasibilityEstimates"

router = APIRouter(prefix="/ordering")


async def _get_feasibility_estimate_cint(
    request: FeasibilityEstimateModel, field_period: int
):
    """
    Cint implementation for retrieving the feasibility_estimate
    """

    headers = {"X-Api-Key": os.getenv("CINT_API_KEY")}
    payload = {
        "lengthOfInterview": request.lengthOfInterview,
        "limit": request.limit,
        "fieldPeriod": field_period,
        "incidenceRate": 100,
        "countryId": 1,
        "quotaGroups": [
            {
                "quotas": [
                    {
                        "limit": request.limit,
                        "targetGroup": {
                            "minAge": 18,
                            "maxAge": 99,
                        },
                    }
                ]
            }
        ],
    }

    async with aiohttp.ClientSession(headers=headers) as session:
        try:
            async with session.post(url=CINT_API_ENDPOINT, json=payload) as response:
                json_response = await response.json()
                if json_response["errors"]:
                    raise HTTPException(
                        response.status,
                        {
                            "status": "failure",
                            "description": {
                                "is_feasible": False,
                                "errors": json_response["errors"],
                            },
                        },
                    )
                else:
                    return json_response

        except aiohttp.client_exceptions.ClientConnectorError:
            raise HTTPException(
                503, f"Cint service is currently unavailable, '{CINT_API_ENDPOINT}'"
            )


@router.get("/feasibility-estimate")
async def get_feasibility_estimate(request: FeasibilityEstimateModel = Depends()):
    """
     - limit: int
     - lengthOfInterview: int

    Returns the shortest feasibile field period from FIELD_PERIOD_PAIRS if feasible
    Stops immediately when there's a feasible period found
     - Skips feasibility requests for longer periods if a shorter one was already found
     - Reduces the amount of API calls needed
    """

    for field_period in sorted(FIELD_PERIOD_PAIRS.keys()):
        response = await _get_feasibility_estimate_cint(request, field_period)
        try:
            feasibility = response["feasibility"]
        except KeyError:
            raise HTTPException(
                501,
                {
                    "status": "failure",
                    "description": "Sorry, no feasibility result is being retrieved from Cint for this estimate. The API may have been updated.",
                },
            )

        if feasibility > 0.75:
            return {
                "detail": {
                    "status": "success",
                    "description": {
                        "is_feasible": True,
                        "deliverable_period": f"Deliverable within a {FIELD_PERIOD_PAIRS[field_period]}",
                    },
                }
            }
    else:
        return {
            "detail": {
                "status": "success",
                "description": {
                    "is_feasible": False,
                    "deliverable_period": f"Not feasible with: {request.limit} people with an interview length of {request.lengthOfInterview}",
                },
            }
        }
