from fastapi import FastAPI

from .v1 import v1_routers

app = FastAPI()
app.include_router(v1_routers.router)


@app.get("/")
def root():
    return {"description": "Welcome to the Focaldata API"}
