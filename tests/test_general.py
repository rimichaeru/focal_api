from helpers import client


def test_domain_root(client):
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"description": "Welcome to the Focaldata API"}
