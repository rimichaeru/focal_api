from typing import Any, Generator

import pytest
from fastapi.testclient import TestClient

from focal_api.main import app


@pytest.fixture()
def client() -> Generator[TestClient, Any, None]:
    yield TestClient(app)
