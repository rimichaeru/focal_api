import pytest
from helpers import client


@pytest.fixture()
def params():
    return {"limit": 10, "lengthOfInterview": 15}


@pytest.mark.parametrize(
    "feasibility",
    (0, 0.2, 0.75),
)
def test_cint_feasibility_estimate_none_feasible(mocker, client, params, feasibility):
    mocker.patch(
        "focal_api.v1.routers.ordering._get_feasibility_estimate_cint",
        return_value={"feasibility": feasibility},
    )

    response = client.get("/v1/ordering/feasibility-estimate", params=params)

    assert response.status_code == 200
    assert response.json() == {
        "detail": {
            "description": {
                "deliverable_period": "Not feasible with: 10 people with an interview length of 15",
                "is_feasible": False,
            },
            "status": "success",
        }
    }


@pytest.mark.parametrize(
    "feasibility",
    (0.76, 1),
)
def test_cint_feasibility_estimate_over_075_feasible(
    mocker, client, params, feasibility
):
    mocker.patch(
        "focal_api.v1.routers.ordering._get_feasibility_estimate_cint",
        return_value={"feasibility": feasibility},
    )

    response = client.get("/v1/ordering/feasibility-estimate", params=params)

    assert response.status_code == 200
    assert response.json() == {
        "detail": {
            "description": {
                "deliverable_period": "Deliverable within a Day",
                "is_feasible": True,
            },
            "status": "success",
        }
    }


def test_cint_feasibility_estimate_multiple_feasible_choose_shortest_day(
    mocker, client, params
):
    mocked_function = mocker.patch(
        "focal_api.v1.routers.ordering._get_feasibility_estimate_cint",
        return_value={"feasibility": 0.76},
    )

    response = client.get("/v1/ordering/feasibility-estimate", params=params)

    assert response.status_code == 200
    assert response.json() == {
        "detail": {
            "description": {
                "deliverable_period": "Deliverable within a Day",
                "is_feasible": True,
            },
            "status": "success",
        }
    }

    mocked_function.assert_called_once()


def test_cint_feasibility_estimate_multiple_feasible_choose_shortest_week(
    mocker, client, params
):
    mocked_function = mocker.patch(
        "focal_api.v1.routers.ordering._get_feasibility_estimate_cint"
    )

    return_values = [
        {"feasibility": 0.75},  # fail
        {"feasibility": 0.76},  # success
        {"feasibility": 0.76},  # success
    ]
    mocked_function.side_effect = return_values

    response = client.get("/v1/ordering/feasibility-estimate", params=params)

    assert response.status_code == 200
    assert response.json() == {
        "detail": {
            "description": {
                "deliverable_period": "Deliverable within a Week",
                "is_feasible": True,
            },
            "status": "success",
        }
    }

    assert mocked_function.call_count == 2


def test_cint_feasibility_estimate_multiple_feasible_choose_shortest_month(
    mocker, client, params
):
    mocked_function = mocker.patch(
        "focal_api.v1.routers.ordering._get_feasibility_estimate_cint"
    )

    return_values = [
        {"feasibility": 0.75},  # fail
        {"feasibility": 0.75},  # fail
        {"feasibility": 0.76},  # success
    ]
    mocked_function.side_effect = return_values

    response = client.get("/v1/ordering/feasibility-estimate", params=params)

    assert response.status_code == 200
    assert response.json() == {
        "detail": {
            "description": {
                "deliverable_period": "Deliverable within a Month",
                "is_feasible": True,
            },
            "status": "success",
        }
    }

    assert mocked_function.call_count == 3


def test_cint_feasibility_estimate_no_feasibility_field_in_response(
    mocker, client, params
):
    """
    This echoes a restriction from Cint, where their API throws a 422 if no within these bounds
    """

    mocker.patch(
        "focal_api.v1.routers.ordering._get_feasibility_estimate_cint", return_value={}
    )

    response = client.get("/v1/ordering/feasibility-estimate", params=params)

    assert response.status_code == 501
    assert response.json() == {
        "detail": {
            "status": "failure",
            "description": "Sorry, no feasibility result is being retrieved from Cint for this estimate. The API may have been updated.",
        }
    }
