# Focaldata API

**!IMPORTANT** For examiners

After running either locally or from docker, the endpoint is available:
 - http://0.0.0.0:8000/docs OR
 - GET http://0.0.0.0:8000/v1/ordering/feasibility-estimate
 - Params can be directly set in the URL, eg. GET http://0.0.0.0:8000/v1/ordering/feasibility-estimate?limit=10&lengthOfInterview=15

## Prerequisties
 - Python (ideally 3.12)
 - Poetry - https://python-poetry.org/docs/

## Setup locally
Note: This could go into a makefile
1. `poetry config virtualenvs.in-project true` - makes it easier to use the .venv to develop locally
2. `poetry install` (if there is an issue with the python version, first create a .venv and activate it, eg. `virtualenv .venv --python=3.12`)
3. `poetry shell`
4. `poetry run pre-commit install`

## Running the app locally
1. `export CINT_API_KEY=API_KEY_HERE` replace API_KEY_HERE with the correct Cint API Key
2. `poetry shell`
3. `poetry run uvicorn focal_api.main:app --reload`
4. Visit the app on `http://0.0.0.0:8000/docs`

## Testing locally
1. After activating the environment/poetry shell
2. `poetry run pytest`

## Build/Run the app locally (container)
1. `export CINT_API_KEY=API_KEY_HERE` replace API_KEY_HERE with the correct Cint API Key
2. `docker build . -t IMAGE_NAME`
3. `docker run -p 8000:8000 IMAGE_NAME`
4. Visit the app on `http://0.0.0.0:8000/docs`
