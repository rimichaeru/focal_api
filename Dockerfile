# Use the official Python image as the base image
FROM python:3.12

ARG CINT_API_KEY
ENV CINT_API_KEY=$CINT_API_KEY

# Set the working directory in the container
WORKDIR /focal_api

# Copy only the files to the container
COPY . /focal_api/

# Install Poetry and dependencies
RUN pip install poetry \
    && poetry config virtualenvs.create false \
    && poetry install --no-dev

# Expose the port on which the Uvicorn server will run
EXPOSE 8000

# Command to run the Uvicorn server
CMD ["poetry", "run", "uvicorn", "focal_api.main:app", "--host", "0.0.0.0"]
